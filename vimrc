execute pathogen#infect()
syntax on
call pathogen#helptags()
set nowrap
set t_Co=256
colorscheme jellybeans
nmap <F5> :NERDTreeToggle<CR>
set number
let g:syntastic_aggregate_errors = 1
nmap <F8> :SyntasticCheck<CR>
let g:syntastic_check_on_open = 1
let g:syntastic_enable_balloons = 1
let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_jump = 0
let g:syntastic_auto_loc_list = 1
set smartindent
set autoindent
if has("autocmd")
	filetype indent on
endif
set vb
"set backupdir=~/.vim/swaps//
let g:rainbow_active = 1
let NERDTreeShowHidden=1
let g:Powerline_symbols = 'fancy'
set mouse=nicr
set mouse=a
nmap <F6> ?def <CR>
nmap <F7> /def <CR>
set noswapfile
